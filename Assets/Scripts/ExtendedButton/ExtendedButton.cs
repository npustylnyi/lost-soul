using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ExtendedButton : Button
{
    public bool isPressed = false;
    public UnityEvent OnEnter;
    public UnityEvent OnExit;
    public UnityEvent OnDown;
    public UnityEvent OnUp;

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
            
        OnEnter?.Invoke();
    }
        
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
            
        OnExit?.Invoke();
    }
        
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
            
        OnDown?.Invoke();
        isPressed = true;
    }
        
    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
            
        OnUp?.Invoke();
        isPressed = false;
    }
}


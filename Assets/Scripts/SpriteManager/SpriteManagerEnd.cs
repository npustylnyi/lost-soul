using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpriteManagerEnd : MonoBehaviour
{
    public int dialogueNumberEnd;
    [SerializeField] private GameObject boyScarySprite;
    [SerializeField] private GameObject girlScarySprite;
    [SerializeField] private GameObject soulHappySprite;
    [SerializeField] private GameObject soulSadSprite;
    [SerializeField] private GameObject soulNormalSprite;
    [SerializeField] private GameObject background;
    
    private void Update()
    {
        if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)
        {
            SpriteObserverEnd();
        }
    }

    private void SpriteObserverEnd()
    {
        if (dialogueNumberEnd == 2)
        {
            boyScarySprite.SetActive(true);
            girlScarySprite.SetActive(true);
            soulNormalSprite.SetActive(true);
            background.SetActive(true);
        }
        
        else if (dialogueNumberEnd == 5)
        {
            boyScarySprite.SetActive(false);
            girlScarySprite.SetActive(false);
            background.SetActive(false);
        }
        
        else if (dialogueNumberEnd == 6)
        {
            soulNormalSprite.SetActive(false);
            soulSadSprite.SetActive(true);
        }
        
        else if (dialogueNumberEnd == 7)
        {
            soulSadSprite.SetActive(false);
            soulHappySprite.SetActive(true);
        }
        
        else if (dialogueNumberEnd == 8)
        {
            soulHappySprite.SetActive(false);
            soulSadSprite.SetActive(true);
        }
        
        else if (dialogueNumberEnd == 11)
        {
            soulSadSprite.SetActive(false);
            soulNormalSprite.SetActive(true);
        }
        
        else if (dialogueNumberEnd == 12)
        {
            soulNormalSprite.SetActive(false);
            soulSadSprite.SetActive(true);
        }
        
        else if (dialogueNumberEnd == 15)
        {
            soulSadSprite.SetActive(false);
            soulHappySprite.SetActive(true);
        }
        
        else if (dialogueNumberEnd == 16)
        {
            SceneManager.LoadScene(0);
        }
    }
}

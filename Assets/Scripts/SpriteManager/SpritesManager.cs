using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpritesManager : MonoBehaviour
{
    public int dialogueNumber;
    [SerializeField] private GameObject boyNormalSprite;
    [SerializeField] private GameObject boySadSprite;
    [SerializeField] private GameObject boyScarySprite;
    [SerializeField] private GameObject boyHappySprite;
    [SerializeField] private GameObject girlNormalSprite;
    [SerializeField] private GameObject girlSadSprite;
    [SerializeField] private GameObject girlScarySprite;
    [SerializeField] private GameObject girlHappySprite;
    [SerializeField] private GameObject fireFlySprite;
    [SerializeField] private GameObject firstBackground;
    [SerializeField] private GameObject secondBackground;
    [SerializeField] private GameObject thirdBackground;
    [SerializeField] private GameObject fourthBackground;

    public AudioManager audioManager;
    
    private void Update()
    {
        if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)
        {
            SpriteObserver();
        }
    }

    private void SpriteObserver()
    {
        if (dialogueNumber == 2)
        {
            audioManager.music.clip = audioManager.roadMusic;
            audioManager.music.Play();
            firstBackground.SetActive(true);
            boyNormalSprite.SetActive(true);
            girlNormalSprite.SetActive(true);
        }
        else if (dialogueNumber == 5)
        {
            boyNormalSprite.SetActive(false);
            girlNormalSprite.SetActive(false);
            boyHappySprite.SetActive(true);
            girlHappySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 6)
        {
            girlHappySprite.SetActive(false);
            girlNormalSprite.SetActive(true);
        }
        
        else if (dialogueNumber == 9)
        {
            girlNormalSprite.SetActive(false);
            girlHappySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 15)
        {
            audioManager.music.Stop();
            audioManager.music.clip = audioManager.houseMusic;
            audioManager.music.Play();
            girlHappySprite.SetActive(false);
            boyHappySprite.SetActive(false);
            firstBackground.SetActive(false);
            girlScarySprite.SetActive(true);
            boyScarySprite.SetActive(true);
            secondBackground.SetActive(true);
            
        }
        
        else if (dialogueNumber == 18)
        {
            boyScarySprite.SetActive(false);
            boyNormalSprite.SetActive(true);
        }
        
        else if (dialogueNumber == 21)
        {
            boyNormalSprite.SetActive(false);
            boyHappySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 23)
        {
            boyHappySprite.SetActive(false);
            boyNormalSprite.SetActive(true);
        }
        
        else if (dialogueNumber == 24)
        {
            girlScarySprite.SetActive(false);
            girlNormalSprite.SetActive(true);
        }
        
        else if (dialogueNumber == 26)
        {
            girlNormalSprite.SetActive(false);
            girlScarySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 30)
        {
            girlScarySprite.SetActive(false);
            girlNormalSprite.SetActive(true);
        }

        else if (dialogueNumber == 36)
        {
            audioManager.music.Stop();
            audioManager.music.clip = audioManager.corridorMusic;
            audioManager.music.Play();
            secondBackground.SetActive(false);
            thirdBackground.SetActive(true);
        }
        
        else if (dialogueNumber == 39)
        {
            girlNormalSprite.SetActive(false);
            boyNormalSprite.SetActive(false);
            girlScarySprite.SetActive(true);
            boyScarySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 40)
        {
            fireFlySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 43)
        {
            audioManager.music.Stop();
            audioManager.music.clip = audioManager.fireflyMusic;
            audioManager.music.Play();
            boyScarySprite.SetActive(false);
            girlScarySprite.SetActive(false);
            thirdBackground.SetActive(false);
            boyNormalSprite.SetActive(true);
            girlNormalSprite.SetActive(true);
            fourthBackground.SetActive(true);
        }
        
        else if (dialogueNumber == 60)
        {
            boyNormalSprite.SetActive(false);
            boyHappySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 63)
        {
            boyHappySprite.SetActive(false);
            boyNormalSprite.SetActive(true);
        }
        
        else if (dialogueNumber == 71)
        {
            boyNormalSprite.SetActive(false);
            boyHappySprite.SetActive(true);
        }
        
        else if (dialogueNumber == 72)
        {
            fourthBackground.SetActive(false);
            boyHappySprite.SetActive(false);
            girlNormalSprite.SetActive(false);
            fireFlySprite.SetActive(false);
        }
        
        else if (dialogueNumber == 74)
        {
            SceneManager.LoadScene(2);
        }
    }
}

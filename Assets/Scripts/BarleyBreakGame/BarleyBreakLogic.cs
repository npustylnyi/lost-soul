using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BarleyBreakLogic : MonoBehaviour
{
    [SerializeField] private List<CellStatus> cellMatrix;
    [SerializeField] private Vector2 matrixSize;
    [SerializeField] private GameObject playerController;
    [SerializeField] private GameObject barleyBreakGame;
    [SerializeField] private Note note;
    private CellStatus cellEmpty;

    private void Start()
    {
        if (CheckCorrectMatrix())
        {
            SetEmptyCell();
            SetMovementCell();
        }
    }

    private bool CheckCorrectMatrix()
    {
        if (matrixSize.x * matrixSize.y != cellMatrix.Count)
        {
            Debug.LogError("cell matrix not equals matrix size");
            return false;
        }
        else
        {
            return true;
        }
    }

    private void SetEmptyCell()
    {
        for (int i = 0; i < cellMatrix.Count; i++)
        {
            if (cellMatrix[i].isEmpty)
            {
                cellEmpty = cellMatrix[i];
            }
        }
    }

    private void SetMovementCell()
    {
        for (int i = 0; i < cellMatrix.Count; i++)
        {
            cellMatrix[i].isMovement = false;
        }

        Vector2 emptyCellPosition = cellEmpty.nowPosition;
        if (emptyCellPosition.x + 1 < matrixSize.x)
        {
            cellMatrix[GetCellIndexInMatrix(new Vector2(emptyCellPosition.x + 1, emptyCellPosition.y))]
                .isMovement = true;
        }

        if (emptyCellPosition.x - 1 >= 0)
        {
            cellMatrix[GetCellIndexInMatrix(new Vector2(emptyCellPosition.x - 1, emptyCellPosition.y))]
                .isMovement = true;
        }

        if (emptyCellPosition.y + 1 < matrixSize.y)
        {
            cellMatrix[GetCellIndexInMatrix(new Vector2(emptyCellPosition.x, emptyCellPosition.y + 1))]
                .isMovement = true;
        }

        if (emptyCellPosition.y - 1 >= 0)
        {
            cellMatrix[GetCellIndexInMatrix(new Vector2(emptyCellPosition.x, emptyCellPosition.y - 1))]
                .isMovement = true;
        }
    }

    private int GetCellIndexInMatrix(Vector2 positionCell)
    {
        int position = (int)(matrixSize.x * positionCell.y + positionCell.x);
        return position;
    }

    public void MoveCell(CellStatus cellStatus)
    {
        if (!cellStatus.isMovement)
        {
            Debug.Log("This cell cant move");
            return;
        }

        CellStatus cell = cellStatus;
        cellMatrix[GetCellIndexInMatrix(cellStatus.nowPosition)] = cellEmpty;
        cellMatrix[GetCellIndexInMatrix(cellEmpty.nowPosition)] = cell;

        Vector2 matrixPosition = cellStatus.nowPosition;
        cellStatus.nowPosition = cellEmpty.nowPosition;
        cellEmpty.nowPosition = matrixPosition;

        Vector3 positionCell = cellStatus.transform.position;
        cellStatus.transform.position = cellEmpty.transform.position;
        cellEmpty.transform.position = positionCell;

        SetEmptyCell();
        SetMovementCell();
        CheckVictory();
    }

    private void CheckVictory()
    {
        bool isWon = true;
        for (int i = 0; i < cellMatrix.Count; i++)
        {
            if (!cellMatrix[i].CellOnCorrentPosition())
            {
                isWon = false;
            }
        }

        if (isWon)
        {
            EndGame();
        }
        else
        {
            Debug.Log("Nope");
        }
    }

    public void StartGame()
    {
        barleyBreakGame.SetActive(true);
        playerController.SetActive(false);
    }

    [ContextMenu("EndGame")]
    public void EndGame()
    {
        Debug.Log("VICTORY!!!!");
        barleyBreakGame.SetActive(false);
        playerController.SetActive(true);
        note.ActivateNote();
    }
}
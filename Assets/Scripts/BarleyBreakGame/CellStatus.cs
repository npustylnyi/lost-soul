using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CellStatus : MonoBehaviour
{
    public Vector2 nowPosition;
    public Vector2 correctPosition;
    public bool isEmpty;
    public bool isMovement;

    public bool CellOnCorrentPosition()
    {
        if (nowPosition == correctPosition)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

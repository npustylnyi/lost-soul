using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShelterBuilder : MonoBehaviour
{
    [SerializeField] private Note note;
    
    [SerializeField] private Transform firstPoint;
    [SerializeField] private Transform secondPoint;

    [SerializeField] private GameObject[] goodToys;
    [SerializeField] private GameObject[] badToys;

    [SerializeField] private int goodToyDropChance;

    [SerializeField] private int countOfItemsInRow;

    [SerializeField] private float minDropSpeed;
    [SerializeField] private float maxDropSpeed;

    private List<GameObject> currentGoodToys;
    private List<GameObject> currentToysInARow;

    [SerializeField] private int minMissingGoodToys;
    [SerializeField] private int minHitsByBadToys;
    private int badToysHit;
    private int missingGoodToys;
    private int collectedGoodToys;
    
    [ContextMenu("StartGame")]
    public void StartGame()
    {
        currentToysInARow = new List<GameObject>();
        badToysHit = 0;
        missingGoodToys = 0;
        collectedGoodToys = 0;
        currentGoodToys = goodToys.ToList();
        for (int i = 0; i < countOfItemsInRow; i++)
        {
            SpawnToy();
        }
    }

    private void SpawnToy()
    {
        GameObject newToy;
        if (currentGoodToys.Count == 0)
        {
            int choosenToy = UnityEngine.Random.Range(0, badToys.Length);
            Vector3 toyStartPosition = Vector3.Lerp(firstPoint.position, secondPoint.position, UnityEngine.Random.Range(0f,1f));
            newToy = Instantiate(badToys[choosenToy], toyStartPosition, Quaternion.identity);
        }
        else
        {
            bool isGoodToy = UnityEngine.Random.Range(0, 100) <= goodToyDropChance;

            if (isGoodToy)
            {
                int choosenToy = UnityEngine.Random.Range(0, currentGoodToys.Count);
                Vector3 toyStartPosition =
                    Vector3.Lerp(firstPoint.position, secondPoint.position, UnityEngine.Random.Range(0f,1f));
                newToy = Instantiate(currentGoodToys[choosenToy], toyStartPosition, Quaternion.identity);
                currentGoodToys.RemoveAt(choosenToy);
            }
            else
            {
                int choosenToy = UnityEngine.Random.Range(0, badToys.Length);
                Vector3 toyStartPosition =
                    Vector3.Lerp(firstPoint.position, secondPoint.position, UnityEngine.Random.Range(0f,1f));
                newToy = Instantiate(badToys[choosenToy], toyStartPosition, Quaternion.identity);
            }
        }

        newToy.GetComponent<Rigidbody2D>().velocity =  new Vector2(0,-UnityEngine.Random.Range(minDropSpeed, maxDropSpeed));

        Toy newToyLogic = newToy.GetComponent<Toy>();
        newToyLogic.OnPlayerHit += PlayerToyHit;
        newToyLogic.OnFloorHit += FloorToyHit;
        currentToysInARow.Add(newToy);
    }

    private void PlayerToyHit(Toy hitterToy)
    {
        hitterToy.OnPlayerHit -= PlayerToyHit;
        hitterToy.OnFloorHit -= FloorToyHit;

        if (!hitterToy.isGoodToy)
        {
            badToysHit += 1;
        }
        else
        {
            collectedGoodToys += 1;
        }
        Destroy(hitterToy.gameObject);
        currentToysInARow.Remove(hitterToy.gameObject);
        SpawnToy();
        CheckGameOver();
    }

    private void FloorToyHit(Toy hitterToy)
    {
        hitterToy.OnPlayerHit -= PlayerToyHit;
        hitterToy.OnFloorHit -= FloorToyHit;

        if (hitterToy.isGoodToy)
        {
            missingGoodToys += 1;
        }
        Destroy(hitterToy.gameObject);
        currentToysInARow.Remove(hitterToy.gameObject);
        SpawnToy();
        CheckGameOver();
    }

    private void CheckGameOver()
    {
        if (missingGoodToys >= minMissingGoodToys || badToysHit >= minHitsByBadToys)
        {
            for (int i = 0; i < currentToysInARow.Count; i++)
            {
                Destroy(currentToysInARow[i]);
            }
            StartGame();
        }

        if (collectedGoodToys == goodToys.Length)
        {
            for (int i = 0; i < currentToysInARow.Count; i++)
            {
                Destroy(currentToysInARow[i]);
            }
            Debug.Log("Victory");
            note.ActivateNote();
        }
    }
}

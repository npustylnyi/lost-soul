using System;
using UnityEngine;

public class Toy : MonoBehaviour
{
    public bool isGoodToy;
    public Action<Toy> OnPlayerHit;
    public Action<Toy> OnFloorHit;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerController>() != null)
        {
            OnPlayerHit?.Invoke(this);
            return;
        }

        else if (other.gameObject.CompareTag("Floor"))
        {
            OnFloorHit?.Invoke(this);
        }

        else
        {
            return;
        }
        
    }
}
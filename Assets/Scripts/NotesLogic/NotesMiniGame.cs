using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NotesMiniGame : MonoBehaviour
{
    [SerializeField] private AudioSource notesSound;
    
    public void TryToStartGame()
    {
        if (NotesManager.Instance.CheckAllNotesCollected())
        {
            SceneManager.LoadScene(7);
        }
    }
}

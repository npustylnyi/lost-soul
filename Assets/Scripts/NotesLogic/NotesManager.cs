using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesManager : MonoBehaviour
{
    public static NotesManager Instance;
    
    private List<bool> notes = new List<bool>(){false,false,false,false};
    private void Awake()
    {
        if (NotesManager.Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public void ConfirmNote(int index)
    {
        notes[index] = true;
    }

    public bool CheckNote(int index)
    {
        return notes[index];
    }

    public bool CheckAllNotesCollected()
    {
        for (int i = 0; i < notes.Count; i++)
        {
            if (!notes[i])
            {
                return false;
            }
        }

        return true;
    }
}

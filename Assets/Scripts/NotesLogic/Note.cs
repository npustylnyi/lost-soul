using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    [SerializeField] private int index;

    public void ActivateNote()
    {
        gameObject.SetActive(true);
        if (NotesManager.Instance.CheckNote(index))
        {
            gameObject.SetActive(false);
        }
    }

    public void ConfirmNote()
    {
        NotesManager.Instance.ConfirmNote(index);
    }
}

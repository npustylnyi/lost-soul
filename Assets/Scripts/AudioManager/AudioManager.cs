using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource music;
    
    public AudioClip roadMusic;
    public AudioClip houseMusic;
    public AudioClip corridorMusic;
    public AudioClip fireflyMusic;

    public SpritesManager spritesManager;
}

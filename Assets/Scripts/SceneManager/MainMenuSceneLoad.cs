using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSceneLoad : MonoBehaviour
{
    [SerializeField] private int sceneNumber;

    public void LoadScene()
    {
        SceneManager.LoadScene(1);
    }
}

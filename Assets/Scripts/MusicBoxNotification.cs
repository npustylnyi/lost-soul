using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBoxNotification : MonoBehaviour
{
    [SerializeField] private GameObject gamePurpose;
    private void OnTriggerEnter2D(Collider2D other)
    {
        gamePurpose.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        gamePurpose.SetActive(false);
    }
}

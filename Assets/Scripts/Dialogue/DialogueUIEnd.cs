using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem.HID;
using UnityEngine.UI;

public class DialogueUIEnd : MonoBehaviour
{
    [SerializeField] private TMP_Text textLabel;
    [SerializeField] private DialogueObject testDialogue;

    [SerializeField] private SpriteManagerEnd spritesManagerEnd;
    
    private TypeWriterEffect typeWriterEffect;

    private void Start()
    {
        typeWriterEffect = GetComponent<TypeWriterEffect>();
        ShowDialogue(testDialogue);
    }
    
    public void ShowDialogue(DialogueObject dialogueObject)
    {
        StartCoroutine(StepThroughDialogue(dialogueObject));
    }

    private IEnumerator StepThroughDialogue(DialogueObject dialogueObject)
    {
        foreach (string dialogue in dialogueObject.Dialogue)
        {
            spritesManagerEnd.dialogueNumberEnd++;
            yield return typeWriterEffect.Run(dialogue, textLabel);
            yield return new WaitUntil(() => (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended));
        }
    }
}

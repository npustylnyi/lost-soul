using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 1.0f;
    public GameObject actionButton;
    public Rigidbody2D playerRB;
    private Vector2 movement;
    private Lever currentLever = null;
   [SerializeField] private ExtendedButton upButton;
   [SerializeField] private ExtendedButton downButton;
   [SerializeField] private ExtendedButton leftButton;
   [SerializeField] private ExtendedButton rightButton;
   [SerializeField] private Animator playerAnimator;
   

   private void Update()
   {
       if(upButton.isPressed ) PlayerMoveUp();
       else if(downButton.isPressed ) PlayerMoveDown();
       else if(leftButton.isPressed ) PlayerMoveLeft();
       else if(rightButton.isPressed) PlayerMoveRight();
       else PlayerIdle();
   }

   public void PlayerMoveUp()
    {
        movement = new Vector2(0, 1);
        playerRB.MovePosition(playerRB.position + movement * moveSpeed * Time.fixedDeltaTime);
        playerAnimator.SetBool("isGoingUp", true);
    }
    
    public void PlayerMoveDown()
    {
        movement = new Vector2(0, -1);
        playerRB.MovePosition(playerRB.position + movement * moveSpeed * Time.fixedDeltaTime);
        playerAnimator.SetBool("isGoingDown", true);
    }
    
    public void PlayerMoveLeft()
    {
        movement = new Vector2(-1, 0);
        playerRB.MovePosition(playerRB.position + movement * moveSpeed * Time.fixedDeltaTime);
        playerAnimator.SetBool("isGoingLeft", true);
    }
    
    public void PlayerMoveRight()
    {
        movement = new Vector2(1, 0);
        playerRB.MovePosition(playerRB.position + movement * moveSpeed * Time.fixedDeltaTime);
        playerAnimator.SetBool("isGoingRight", true);
    }

    public void PlayerIdle()
    {
        playerAnimator.SetBool("isGoingUp", false);
        playerAnimator.SetBool("isGoingDown", false);
        playerAnimator.SetBool("isGoingLeft", false);
        playerAnimator.SetBool("isGoingRight", false);
    }
    
    
    private void OnTriggerEnter2D(Collider2D other)
    {
       currentLever = other.GetComponent<Lever>();
        if (currentLever != null)
        {
            Debug.Log("enter");
            actionButton.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (currentLever == other.GetComponent<Lever>())
        {
            Debug.Log("exit");
            actionButton.SetActive(false);
            currentLever = null;
        }
    }

    public void InteractWithLever()
    {
        if (currentLever == null)
        {
            return;
        }
        currentLever.OnLeverClick();
    }
    
}

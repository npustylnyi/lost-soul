using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObserver : MonoBehaviour
{
    public Transform player;
    public float speed;

    private void FixedUpdate()
    {
        Vector2 cameraPosition = Vector2.Lerp(transform.position, player.position, speed * Time.deltaTime);
        transform.position = new Vector3(cameraPosition.x, cameraPosition.y, transform.position.z);
    }
}

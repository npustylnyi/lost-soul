using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockpickSideButton : MonoBehaviour
{
    [SerializeField] private LockSides side;
    [SerializeField] private LockpickCombination lockpickCombination;

    public void HandleClick()
    {
        lockpickCombination.AddCombinationSide(side);
    }
}

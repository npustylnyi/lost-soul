using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockpickCombination : MonoBehaviour
{
    [SerializeField] private LockSides[] firstCombination;
    [SerializeField] private LockSides[] secondCombination;
    [SerializeField] private LockSides[] thirdCombination;
    [SerializeField] private int currentIndex = 0;
    [SerializeField] private GameObject player;
    [SerializeField] private Note note;
    [SerializeField] private Button topButton;
    [SerializeField] private Button bottomButton;
    [SerializeField] private Button leftButton;
    [SerializeField] private Button rightButton;
    [SerializeField] private Button playButton;
    [SerializeField] private Image topButtonView;
    [SerializeField] private Image bottomButtonView;
    [SerializeField] private Image leftButtonView;
    [SerializeField] private Image rightButtonView;
    [SerializeField] private GameObject wrongCombinationText;
    [Space(10)] 
    [SerializeField] private Sprite topYellowButton;
    [SerializeField] private Sprite bottomYellowButton;
    [SerializeField] private Sprite leftYellowButton;
    [SerializeField] private Sprite rightYellowButton;
    [Space(10)]
    [SerializeField] private Sprite topRedButton;
    [SerializeField] private Sprite bottomRedButton;
    [SerializeField] private Sprite leftRedButton;
    [SerializeField] private Sprite rightRedButton;
    [Space(10)] 
    [SerializeField] private Sprite topGrayButton;
    [SerializeField] private Sprite bottomGrayButton;
    [SerializeField] private Sprite leftGrayButton;
    [SerializeField] private Sprite rightGrayButton;
    
    [SerializeField] private List<LockSides> currentCombination = new List<LockSides>();

    public void AddCombinationSide(LockSides side)
    {
        GetNeededCombination(side);
    }

    private void GetNeededCombination(LockSides side)
    {
        if (currentIndex == 0)
        {
            ValidateCombination(firstCombination, side);
            return;
        }

        if (currentIndex == 1)
        {
            ValidateCombination(secondCombination, side);
            return;
        }
        
        if (currentIndex == 2)
        {
            ValidateCombination(thirdCombination, side);
        }

        if (currentIndex == 3)
        {
            Victory();
        }
        
    }

    public void Victory()
    {
        Debug.Log("Victory");
        player.SetActive(true);
        gameObject.SetActive(false);
        note.ActivateNote();
    }

    private void ValidateCombination(LockSides[] combination, LockSides side)
    {
        currentCombination.Add(side);
        wrongCombinationText.SetActive(false);
        if (combination[currentCombination.Count - 1] != side)
        {
            currentCombination.Clear();
            wrongCombinationText.SetActive(true);
            StartCoroutine(WrongCombinationAhtung());
        }

        if (currentCombination.Count == combination.Length)
        {
            Debug.Log("все збс");
            currentCombination.Clear();
            currentIndex++;
            ViewCombination();
        }
    }

    private IEnumerator WrongCombinationAhtung()
    {
        topButtonView.sprite = topRedButton;
        bottomButtonView.sprite = bottomRedButton;
        leftButtonView.sprite = leftRedButton;
        rightButtonView.sprite = rightRedButton;
        yield return new WaitForSeconds(0.5f);
        topButtonView.sprite = topGrayButton;
        bottomButtonView.sprite = bottomGrayButton;
        leftButtonView.sprite = leftGrayButton;
        rightButtonView.sprite = rightGrayButton;
    }

    public void StarGame()
    {
        player.SetActive(false);
        gameObject.SetActive(true);
        ViewCombination();
    }

    public void ViewCombination()
    {
        if (currentIndex == 0)
        {
            StartCoroutine(ShowCombination(firstCombination));
        }

        if (currentIndex == 1)
        {
            StartCoroutine(ShowCombination(secondCombination));
        }
        
        if (currentIndex == 2)
        {
            StartCoroutine(ShowCombination(thirdCombination));
        }
    }

    private IEnumerator ShowCombination(LockSides[] combination)
    {
        topButton.interactable = false;
        bottomButton.interactable = false;
        leftButton.interactable = false;
        rightButton.interactable = false;
        playButton.interactable = false;
        for (int i = 0; i < combination.Length; i++)
        {
            topButtonView.sprite = topGrayButton;
            bottomButtonView.sprite = bottomGrayButton;
            leftButtonView.sprite = leftGrayButton;
            rightButtonView.sprite = rightGrayButton;
            switch (combination[i])
            {
                case LockSides.Top:
                    topButtonView.sprite = topYellowButton;
                    break;
                case LockSides.Bottom:
                    bottomButtonView.sprite = bottomYellowButton;
                    break;
                case LockSides.Left:
                    leftButtonView.sprite = leftYellowButton;
                    break;
                case LockSides.Right:
                    rightButtonView.sprite = rightYellowButton;
                    break;
            }

            yield return new WaitForSeconds(0.5f);
            topButtonView.sprite = topGrayButton;
            bottomButtonView.sprite = bottomGrayButton;
            leftButtonView.sprite = leftGrayButton;
            rightButtonView.sprite = rightGrayButton;
            yield return new WaitForSeconds(0.5f);
        }
        topButtonView.sprite = topGrayButton;
        bottomButtonView.sprite = bottomGrayButton;
        leftButtonView.sprite = leftGrayButton;
        rightButtonView.sprite = rightGrayButton;
        topButton.interactable = true;
        bottomButton.interactable = true;
        leftButton.interactable = true;
        rightButton.interactable = true;
        playButton.interactable = true;
    }
}

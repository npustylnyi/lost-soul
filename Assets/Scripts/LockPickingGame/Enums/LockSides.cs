using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LockSides
{
    Left = 0,
    Right,
    Top,
    Bottom
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverObserver : MonoBehaviour
{
   [SerializeField] private List<Lever> levers = new List<Lever>();

   private void OnEnable()
   {
      foreach (var lever in levers)
      {
         lever.OnClick += OnLeverClick;
      }
   }
   
   private void OnDisable()
   {
      foreach (var lever in levers)
      {
         lever.OnClick -= OnLeverClick;
      }
   }

   private void OnLeverClick(Lever lever)
   {
      int index = levers.IndexOf(lever);
      if (index + 1 == levers.Count)
      {
         Debug.Log("LevelComplete");
      }
      else
      {
         levers[index+1].gameObject.SetActive(true);
      }
   }
}

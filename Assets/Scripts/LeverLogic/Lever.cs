using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class Lever : MonoBehaviour
{
    public Action <Lever> OnClick;
    public UnityEvent onUnityClick = new UnityEvent();
    
   public void OnLeverClick()
   {
         Debug.Log("Кнопка нажата");
         gameObject.SetActive(false);
         OnClick?.Invoke(this);
         onUnityClick?.Invoke();
   }
}
 